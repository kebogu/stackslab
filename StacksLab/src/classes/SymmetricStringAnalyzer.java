package classes;

public class SymmetricStringAnalyzer {
	private String s; 
	public SymmetricStringAnalyzer(String s) {
		this.s = s; 
	}
	
	/**
	 * Determines if the string s is symmetric
	 * @return true if it is; false, otherwise. 
	 */
	public boolean isValidContent() { 
		// ADD MISSING CODE
		SLLStack<String> newStack = new SLLStack<String>();
		for (int i=0; i < s.length(); i++) 
		{ 
	        char c = s.charAt(i); 
	        if (Character.isLetter(c))
	        {
	           if(Character.isUpperCase(c))    
	        	   newStack.push(Character.toString(c));
	           
	           else 
	        	   if (Character.isLowerCase(c)) 
	        	   {
	        		   if(newStack.isEmpty())
	        		   {
	        			   return false;
	        		   }
	        		   
	        		   else
	        		   {
			                String lowerHolder = newStack.pop();              
			                if(!(lowerHolder.toLowerCase().compareTo(Character.toString(c)) == 0))
			                {
			                	return false;
			                } 	
	        		   }
	        	   }
	        }
            else 
            	return false; 
	    } 
		if(newStack.isEmpty())
		{
			return true;
	    }
		return false;
	}

	
	
	
	public String toString() { 
		return s; 
	}

	public String parenthesizedExpression() 
	throws StringIsNotSymmetricException 
	{
		// ADD MISSING CODE
		SLLStack<String> newestStack = new SLLStack<String>();
		String theString = "";
		if(!this.isValidContent())
		{
			throw new StringIsNotSymmetricException() ;
		}
		
		for (int i = this.s.length()-1; i >= 0; i--) {
		 
			if(Character.isUpperCase(this.s.charAt(i)))
			{
				newestStack.push("<"+this.s.charAt(i));
			}
			else 
			{
				newestStack.push(this.s.charAt(i)+">");
			}
		}
		for (int i = 0; i < this.s.length()-1; i++) 
		{
			theString = theString + newestStack.pop();
		}
		return theString;  // need to change if necessary....
	}

}
