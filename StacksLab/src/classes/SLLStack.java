package classes;

import interfaces.Stack;

public class SLLStack<E> implements Stack<E>
{   
	// SNode is a public class defined elsewhere. No 
	// need to have it as a private class in this case. 
	private SNode<E> top;   
	private int size; 
	
	public SLLStack() {
		top = null; 
		size = 0; 
	}
	
	public E pop() {
		if (isEmpty())	return null; 
		// ADD CODE TO COMPLETE IMPLEMENTATION
		/*
		 E result = this.header.getNext().getElement();
			Node<E> temp = this.header.getNext();
			this.header.setNext(temp.getNext());
			temp.setNext(null);
			temp.setElement(null);
			this.currentSize--;
			return result;
		 */
		E etr = this.top.getElement(); 
		SNode<E> mr = this.top;
		this.top = this.top.getNext();
		mr.clean();
		this.size--;
		
		
		return etr;
	}

	public void push(E e) {
		// ADD CODE TO COMPLETE IMPLEMENTATION
		/*
		 Node<E> newNode = new Node<>();
		newNode.setElement(e);
		newNode.setNext(this.header.getNext());
		this.header.setNext(newNode);
		this.currentSize++;
		return; 	  
		 */
		SNode<E> cheekibreeki = new SNode<E>();
		cheekibreeki.setElement(e);
		if(this.isEmpty())
		{
			this.top = cheekibreeki;
			this.size++;
		}
		else {
			cheekibreeki.setNext(this.top);
			this.top = cheekibreeki;
			this.size++;
		}
		
	}

	public E top() {
		if (isEmpty())
			return null;  
		return top.getElement();
	}

	public int size() {
		return size;
	}

	public boolean isEmpty() {
		return size == 0;
	}


	// The following two methods are just for testing and grading....
    public void showReverse() { 
	    if (size == 0)
		   System.out.println("Stack is empty."); 
		else
		   recSR(top);
    }
    private void recSR(SNode<E> f) { 
		if (f != null) { 
		   recSR(f.getNext()); 
		   System.out.println(f.getElement()); 
	     } 
    } 
 
}
